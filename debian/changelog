ruby-thor (0.20.3-2) UNRELEASED; urgency=medium

  * Add salsa-ci.yml

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Tue, 13 Aug 2019 07:58:56 +0530

ruby-thor (0.20.3-1) experimental; urgency=medium

  * Team upload
  * New upstream version 0.20.3
    + Add test suite
  * d/compat: Bump debhelper compatibility level to 11
  * d/control:
    + Use salsa.debian.org in Vcs-* fields
    + Bump Standards-Version to 4.3.0 (no changes needed)
  * d/copyright: Fix lintian P: insecure-copyright-format-uri
  * d/patches:
    + Exclude test which uses git
    + Remove obsolete git-version-fix
    + Patch for excluding the binaries
  * d/watch: Move to github.com/erikhuda/thor for importing tests

 -- Jongmin Kim <jmkim@pukyong.ac.kr>  Wed, 12 Jun 2019 21:40:28 +0900

ruby-thor (0.19.4-1) unstable; urgency=medium

  * Team upload
  * New upstream version 0.19.4
  * Drop patch disable-some-tests
  * Refresh packaging using dh-make-ruby
  * Add myself to Uploaders

 -- Lucas Nussbaum <lucas@debian.org>  Sun, 02 Jul 2017 10:05:11 +0200

ruby-thor (0.19.1-3) unstable; urgency=medium

  * Team upload.

  [ Cédric Boutillier ]
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields

  [ Christian Hofstaedtler ]
  * Remove myself from Uploaders
  * Bump Standards-Version to 3.9.8

 -- Christian Hofstaedtler <zeha@debian.org>  Wed, 30 Nov 2016 18:06:26 +0000

ruby-thor (0.19.1-2) unstable; urgency=medium

  * Disable testsuite as it's completely broken now (Closes: #795717)
  * Refresh packaging with dh-make-ruby -w

 -- Christian Hofstaedtler <zeha@debian.org>  Tue, 18 Aug 2015 22:11:03 +0000

ruby-thor (0.19.1-1) unstable; urgency=medium

  * New upstream release.

 -- Christian Hofstaedtler <zeha@debian.org>  Sat, 29 Mar 2014 17:21:32 +0100

ruby-thor (0.18.1.git20140116-2) unstable; urgency=medium

  * Actually have Gem version be higher than latest release

 -- Christian Hofstaedtler <zeha@debian.org>  Thu, 16 Jan 2014 14:31:40 +0100

ruby-thor (0.18.1.git20140116-1) unstable; urgency=medium

  * Imported Upstream version 0.18.1.git20140116
  * Update my e-mail address
  * Bump Standards-Version to 3.9.5 (no changes)
  * Refresh patches

 -- Christian Hofstaedtler <zeha@debian.org>  Thu, 16 Jan 2014 12:36:16 +0100

ruby-thor (0.18.1-1) unstable; urgency=low

  [ Ondřej Surý ]
  * Remove myself from Uploaders:

  [ Christian Hofstaedtler ]
  * New upstream version.
  * Remove obsolete DM-Upload-Allowed flag
  * Bump Standards-Version to 3.9.4
  * Update debian/copyright Format URL
  * Set dpkg feature "unapply-patches"
  * Add myself to Uploaders:, so there is a human maintainer
  * Use canonical URLs in Vcs-* fields
  * Add a longer package description
  * Run test suite during package build (implies new Build-Depends)
  * Add man page for thor(1)

 -- Christian Hofstaedtler <zeha@debian.org>  Tue, 07 May 2013 17:15:18 +0200

ruby-thor (0.15.3-1) unstable; urgency=low

  * Team upload.
  * New upstream version.
  * Bump build dependency on gem2deb to >= 0.3.0~.

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Wed, 27 Jun 2012 21:42:53 +0200

ruby-thor (0.14.6-2) unstable; urgency=low

  [ Laurent Bigonville ]
  * debian/copyright: Dep5 requires that a reference to an already included
    licence is standalone

  [ Antonio Terceiro ]
  * debian/copyright: fix URL for the sources
  * Bump build dependency on gem2deb to >= 0.3.0~

 -- Laurent Bigonville <bigon@debian.org>  Fri, 11 May 2012 15:44:42 +0200

ruby-thor (0.14.6-1) unstable; urgency=low

  * Initial release (Closes: #647068)

 -- Ondřej Surý <ondrej@debian.org>  Wed, 25 Apr 2012 12:36:56 +0000
